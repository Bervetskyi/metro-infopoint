class MetroInfopoint
  def initialize(path_to_timing_file:, path_to_lines_file:)
    # path_to_file = "./config/timing#{ENV['VARIANT']}.yml"
    # timing_data = YAML.load_file(path_to_file)['timing']
  end

  def calculate(from_station:, to_station:)
    { price: calculate_price(from_station: from_station, to_station: to_station),
      time: calculate_time(from_station: from_station, to_station: to_station) }
  end

  def calculate_price(from_station:, to_station:)
    # your implementation here
  end

  def calculate_time(from_station:, to_station:)
    # your implementation here
  end
end
