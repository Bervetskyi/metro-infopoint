def only_even (array)
  result = []
  i = 0
  loop do
    number = array[i]

    if number % 2 == 0
      result << number
    end

    i += 1

    if i == array.size
      break
    end
  end
  return result
end

array = [1, 2, 3, 4, 8, 9]
print only_even(array)

