require 'yaml'
require 'pp'
path_to_file = "./config/timing3.yml"
timing_data = YAML.load_file(path_to_file)['timing']

pp timing_data
puts "=========================================\n\n"
puts "=========================================\n\n"

def get_end_stations(timing_data, station_name)
  result = []


  i = 0
  loop do
    section = timing_data[i]
    start_station = section["start"]
    end_station = section['end']

    if start_station == station_name
      result<<end_station
    end

    i += 1

    if i == timing_data.size
      break
    end
  end
  return result
end

# puts "End stations for nezalezhna:"
# result = get_end_stations(timing_data,:nezalezhna)
# puts result
#
# puts "End stations for konotopska:"
# result= get_end_stations(timing_data,:konotopska)
# puts result



start_station = :donetska
end_station = :granychna

changes = 0

reachable_stations = [start_station]

loop do
  new_reachable_stations = []

  reachable_stations.each do |reachable_station|
    new_reachable_stations += get_end_stations(timing_data, reachable_station)
  end

  changes += 1

  puts new_reachable_stations.inspect
  puts "----------------------------------"

  if new_reachable_stations.include?(end_station)
    puts "Пересадок: #{changes}"
    break
  else
    reachable_stations = new_reachable_stations
  end


end

