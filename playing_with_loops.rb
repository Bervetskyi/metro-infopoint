def loops(array)
  result = []
  i = 0
  loop do


    number = array[i]
    if i % 2 == 0
      result << number
    end
    i += 1
    if i == array.size
      break
    end

  end
  return result
end

array = [99, 100, 101, 102, 103]
print loops(array)